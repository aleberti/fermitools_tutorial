import gt_apps as my_apps

my_apps.expMap['evfile']    = 'TXS0506+056_select_gti.fits'
my_apps.expMap['scfile']    = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.expMap['expcube']   = 'TXS0506+056_ltcube.fits'
my_apps.expMap['outfile']   = 'TXS0506+056_expmap.fits'
my_apps.expMap['irfs']      = 'P8R3_SOURCE_V2'
my_apps.expMap['srcrad']    = 30
my_apps.expMap['nlong']     = 120
my_apps.expMap['nlat']      = 120
my_apps.expMap['nenergies'] = 20

print(my_apps.expMap.command())

my_apps.expMap.run()
