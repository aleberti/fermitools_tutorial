import gt_apps as my_apps

my_apps.evtbin['algorithm'] = 'CMAP'
my_apps.evtbin['emin']      = 100
my_apps.evtbin['emax']      = 300000
my_apps.evtbin['evfile']    = 'TXS0506+056_select_gti.fits'
my_apps.evtbin['scfile']    = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.evtbin['outfile']   = 'TXS0506+056_cmap.fits'
my_apps.evtbin['nxpix']     = 300
my_apps.evtbin['nypix']     = 300
my_apps.evtbin['binsz']     = 0.1
my_apps.evtbin['coordsys']  = 'CEL'
my_apps.evtbin['xref']      = 77.3582
my_apps.evtbin['yref']      = 5.69315
my_apps.evtbin['axisrot']   = 0
my_apps.evtbin['proj']      = 'AIT'

my_apps.evtbin.run()
