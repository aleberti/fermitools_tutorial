import matplotlib.pyplot as plt
import astropy.io.fits as pyfits

residHDU = pyfits.open('TXS0506+056_tsmap_residual.fits')
sourceHDU = pyfits.open('TXS0506+056_tsmap_source.fits')

fig = plt.figure(figsize=(16,8))

plt.subplot(1,2,1)
plt.imshow(residHDU[0].data)
plt.colorbar()

plt.subplot(1,2,2)
plt.imshow(sourceHDU[0].data)
plt.colorbar()

plt.show()
