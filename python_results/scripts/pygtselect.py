import gt_apps as my_apps

my_apps.filter['evclass'] = 128
my_apps.filter['evtype']  = 3
my_apps.filter['ra']      = 77.3582
my_apps.filter['dec']     = 5.69315
my_apps.filter['rad']     = 15
my_apps.filter['emin']    = 100
my_apps.filter['emax']    = 300000
my_apps.filter['zmax']    = 90
my_apps.filter['tmin']    = 333244802
my_apps.filter['tmax']    = 333849602
my_apps.filter['infile']  = './photon/L20040515476099D9076805_PH00.fits'
my_apps.filter['outfile'] = 'TXS0506+056_select.fits'

print(my_apps.filter.command())

my_apps.filter.run()
