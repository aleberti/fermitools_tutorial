import gt_apps as my_apps

my_apps.TsMap['statistic'] = "UNBINNED"
my_apps.TsMap['scfile']    = "./spacecraft/L20040515476099D9076805_SC00.fits"
my_apps.TsMap['evfile']    = "TXS0506+056_select_gti.fits"
my_apps.TsMap['expmap']    = "TXS0506+056_expmap.fits"
my_apps.TsMap['expcube']   = "TXS0506+056_ltcube.fits"
my_apps.TsMap['srcmdl']    = "TXS0506+056_tsmap_source.xml"
my_apps.TsMap['irfs']      = "P8R3_SOURCE_V2"
my_apps.TsMap['optimizer'] = "NEWMINUIT"
my_apps.TsMap['outfile']   = "TXS0506+056_tsmap_source.fits"
my_apps.TsMap['nxpix']     = 40
my_apps.TsMap['nypix']     = 40
my_apps.TsMap['binsz']     = 0.5
my_apps.TsMap['coordsys']  = "CEL"
my_apps.TsMap['xref']      = 77.3582
my_apps.TsMap['yref']      = 5.69315
my_apps.TsMap['proj']      = 'AIT'

my_apps.TsMap.run()
