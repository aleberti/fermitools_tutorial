import gt_apps as my_apps

my_apps.diffResps['evfile'] = 'TXS0506+056_select_gti.fits'
my_apps.diffResps['scfile'] = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.diffResps['srcmdl'] = 'TXS0506+056_input_model.xml'
my_apps.diffResps['irfs']   = 'P8R3_SOURCE_V2'

print(my_apps.diffResps.command())

my_apps.diffResps.run()
