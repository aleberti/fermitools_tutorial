import gt_apps as my_apps

my_apps.expCube['evfile']    = 'TXS0506+056_select_gti.fits'
my_apps.expCube['scfile']    = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.expCube['outfile']   = 'TXS0506+056_ltcube.fits'
my_apps.expCube['zmax']      = 90
my_apps.expCube['dcostheta'] = 0.025
my_apps.expCube['binsz']     = 1

print(my_apps.expCube.command())

my_apps.expCube.run()
