import gt_apps as my_apps

my_apps.maketime['scfile']  = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.maketime['filter']  = '(DATA_QUAL>0)&&(LAT_CONFIG==1)'
my_apps.maketime['roicut']  = 'no'
my_apps.maketime['evfile']  = 'TXS0506+056_select.fits'
my_apps.maketime['outfile'] = 'TXS0506+056_select_gti.fits'

print(my_apps.maketime.command())

my_apps.maketime.run()
