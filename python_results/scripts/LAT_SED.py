from likeSED import *
import pyLikelihood
from UnbinnedAnalysis import *

obs = UnbinnedObs('TXS0506+056_select_gti.fits','./spacecraft/L20040515476099D9076805_SC00.fits', expMap='TXS0506+056_expmap.fits', expCube='TXS0506+056_ltcube.fits', irfs='P8R3_SOURCE_V2')
like = UnbinnedAnalysis(obs,'TXS0506+056_SED_model.xml',optimizer='NewMinuit')

inputs = likeInput(like, 'TXS 0506+056', nbins=5)
inputs.plotBins()

inputs.fullFit(CoVar=True)
sed = likeSED(inputs)
sed.getECent()
sed.fitBands()
sed.Plot(plot=False)
