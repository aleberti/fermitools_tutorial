# Fermitools tutorial

This repository contains slides and a tutorial for the Fermi Science Tools. This is NOT an official product of the Fermi Software/Science team.

There are introductory slides explaining how likelihood analysis works, see [Fermitools\_tutorial\_likelihood\_analysis.pdf](./slides/Fermitools_tutorial_likelihood_analysis.pdf).

There are two tutorials:
- [unbinned\_likelihood\_tutorial.md](unbinned_likelihood_tutorial.md), showing how to perform an unbinned likelihood analysis
- [likelihood\_tutorial\_python.md](likelihood_tutorial_python.md), showing how to perform an unbinned likelihood analysis using Python.

For these tutorials, there are corresponding hands-on slides:
- [Fermitools\_tutorial\_likelihood\_analysis\_handson.pdf](./slides/Fermitools_tutorial_likelihood_analysis_handson.pdf) for the unbinned likelihood analysis tutorial
- [Fermitools\_tutorial\_likelihood\_analysis\_python\_handson.pdf](./slides/Fermitools_tutorial_likelihood_analysis_python_handson.pdf) for the unbinned likelihood analysis with Python tutorial.
