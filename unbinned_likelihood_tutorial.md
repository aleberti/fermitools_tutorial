# Unbinned Likelihood Tutorial #

## Introduction ##

As seen in the theoretical introduction to the likelihood analysis used to analyze Fermi-LAT data, there are two types of likelihood analyses:

* unbinnned likelihood analysis
* binned likelihood analysis

Since we are going to analyze just a week of Fermi-LAT data, where we do not expect to have many events, we will use the unbinned likelihood analysis. Here, the [specific analysis thread](https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/likelihood_tutorial.html) from the Fermi-LAT web pages will be followed.

## The source ##

In this tutorial, we will analyze the data from a very interesting source, called TXS 0506+056, or 4FGL J0509.4+0542 (from the LAT 8 year source catalog, called 4FGL). It is a blazar at redshift z=0.3365 and it is associated with the event IC-170922A, a high energy astrophysical neutrino detected by Icecube on September 2017, after the observation of high-energy and very-high-energy emission by different instruments. Being the first case of high confidence association of a gamma-ray source with an astrophysical neutrino, the source is being studied in detail, looking at possible high flux flaring states and studying its multi-messenger emission (photons+neutrino) to find suitable physical models. See [here](https://arxiv.org/abs/1807.08816) the discovery paper appeared in Science in July 2018.

This discovery has a high impact on the search of the sources of astrophysical neutrinos and has an intriguing implication on the origin of ultra high energy cosmic rays (UHECRs). It is interesting to see if TXS-like sources may be as well neutrino emitters, shedding light on the origin of astrophysical neutrinos, which is one of the hot topics of moder astroparticle physics. Being the only source associated with a high energy neutrino, it is difficult to draw firm conclusions. Nonetheless, it surely helped putting scientist on a particular path which hopefully will lead to put an important piece to this intriguing puzzle.

## A bit of nomenclature ##

During the tutorial, there will be few recurring terms which can be useful to define here:

* FT1 file or events/photon file: it is the FITS file containing the photon events
* FT2 file or spacecraft file: it is the file containing the information of the spacecraft (position, pointing direction ecc..)
* ROI (Region Of Interest): it is the circular region selected when downloading the data, which can be modified during the analysis
* Source region: it is the circular region where we are going to consider the sources (point-like or extended) which can contribute to the counts in the ROI. Usually it is bigger than the ROI

## Preparation to data analysis: download the data ##

Needless to say, to analyze data, you need the data :) If you did not do already, we will download the data from the [LAT Data Server](https://fermi.gsfc.nasa.gov/cgi-bin/ssc/LAT/LATDataQuery.cgi). To have the same dataset as the one used in this tutorial, the settings of the query are the following:

* Object name or coordinates: TXS0506+056
* Coordinate system: J2000
* Search radius (degrees): 30
* Observation dates: 2011-07-25 00:00:00, 2011-08-01 00:00:00
* Time system: Gregorian
* Energy range (MeV): 100, 300000
* LAT data type: Photon
* Spacecraft data: Checked

After this, click on `Start search` and wait for the query to complete, it should take some seconds. After that, you will see a page with the results of your query. Since we requested both photon and spacecraft files, the result is two files. At the bottom of the page there are the commands (simple `wget`) you should type in your terminal to download the requested data.

**NB:** since we are requesting only one week of data, only two files are retrieved from the query. In the case of data on a longer period of time, usually the photon events are split in several files, while the spacecraft file is only one, but with large size.

To have everything in order, create a directory somewhere (e.g. your home, or a directory where you put all your analyses) which will contain the results of our analysis. Let's call it TXS0506+056:

```
[you@your_super_machine] $ mkdir TXS0506+056
[you@your_super_machine] $ cd TXS0506+056
```

Also, we can create a directory containing the photons file and one for the spacecraft file:

```
[you@your_super_machine] $ mkdir photon spacecraft

[you@your_super_machine] $ wget https://fermi.gsfc.nasa.gov/FTP/fermi/data/lat/queries/L20040515476099D9076805_PH00.fits
--2020-04-05 21:48:37--  https://fermi.gsfc.nasa.gov/FTP/fermi/data/lat/queries/L20040515476099D9076805_PH00.fits
Resolving fermi.gsfc.nasa.gov (fermi.gsfc.nasa.gov)... 129.164.179.26, 2001:4d0:2310:150::26
Connecting to fermi.gsfc.nasa.gov (fermi.gsfc.nasa.gov)|129.164.179.26|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 11067840 (11M) [application/fits]
Saving to: ‘L20040515476099D9076805_PH00.fits’

100%[========================================================================================================>] 11,067,840   598KB/s   in 19s

2020-04-05 21:48:56 (583 KB/s) - ‘L20040515476099D9076805_PH00.fits’ saved [11067840/11067840]

[you@your_super_machine] $ wget https://fermi.gsfc.nasa.gov/FTP/fermi/data/lat/queries/L20040515476099D9076805_SC00.fits
--2020-04-05 21:48:59--  https://fermi.gsfc.nasa.gov/FTP/fermi/data/lat/queries/L20040515476099D9076805_SC00.fits
Resolving fermi.gsfc.nasa.gov (fermi.gsfc.nasa.gov)... 129.164.179.26, 2001:4d0:2310:150::26
Connecting to fermi.gsfc.nasa.gov (fermi.gsfc.nasa.gov)|129.164.179.26|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2649600 (2.5M) [application/fits]
Saving to: ‘L20040515476099D9076805_SC00.fits’

100%[========================================================================================================>] 2,649,600    536KB/s   in 5.9s

2020-04-05 21:49:06 (440 KB/s) - ‘L20040515476099D9076805_SC00.fits’ saved [2649600/2649600]

[you@your_super_machine] $ mv L20040515476099D9076805_PH00.fits photon
[you@your_super_machine] $ mv L20040515476099D9076805_SC00.fits spacecraft
```

Before proceeding further, we need to setup the Fermitools. Having installed them through Conda, we just need to do:

```
[you@your_super_machine] $ conda activate fermi
```

With the data at hand, we can proceed with the selection of data. **NB:** the name of the photon and spacecraft files are probably different for you. So do not copy/paste but do it yourself!

## Data selection ##

As in any data analysis, one can filter the data according to some selection rules. The executable of the Fermi Science Tools responsible for the data selection is `gtselect`. Here we want to reduce the size of the ROI from 30deg to 15deg of radius, keeping everything else fixed. Also we want to select events of the `SOURCE` class, and from this class we want to keep all events, which means selecting the event types `Front` and `Back`.

As any executable from the Fermi Science Tools, `gtselect` is interactive, that is a prompt will ask you to enter information to be feed to the tool. Beside this mode, there is also a pure command line mode where the different parameters can be specified as command line arguments, which lets the tools to be called easily from scripts. You can find the reference documentation for the Fermi Science Tools at the link [https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/references.html](https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/references.html).

Looking at the documentation of `gtselect` ([https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/gtselect.txt](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/gtselect.txt)), the parameters for the event class and event type are 'hidden', so we must specify them as command line arguments. Also, the documentation tells us how we should specify those two parameters. The `SOURCE` class corresponds to `evclass=128`, while the `Front+Back` corresponds to `evtype=3` (see also tables in [https://fermi.gsfc.nasa.gov/ssc/data/analysis/documentation/Cicerone/Cicerone\_LAT\_IRFs/IRF\_overview.html](https://fermi.gsfc.nasa.gov/ssc/data/analysis/documentation/Cicerone/Cicerone_LAT_IRFs/IRF_overview.html))

We can now proceed to call `gtselect`:

```
[you@your_super_machine] $ gtselect evclass=128 evtype=3

Input FT1 file[] ./photon/L20040515476099D9076805_PH00.fits
Output FT1 file[] TXS0506+056_select.fits
RA for new search center (degrees) (0:360) [INDEF] 77.3582
Dec for new search center (degrees) (-90:90) [INDEF] 5.69315
radius of new search region (degrees) (0:180) [INDEF] 15
start time (MET in s) (0:) [INDEF]
end time (MET in s) (0:) [INDEF]
lower energy limit (MeV) (0:) [30] 100
upper energy limit (MeV) (0:) [300000] 300000
maximum zenith angle value (degrees) (0:180) [180] 90
Done.
```

In the different prompts, if nothing is specified, the values will be taken from what is between square brackets. `INDEF` means that the value from the input photon file will be taken, without modifying it.

Here, we specified the coordinates of TXS0506+056 (the same as shown in the query results from the LAT Data Server), we reduced the ROI to 15deg of radius and put a maximum zenith angle cut according to the recommendations of the LAT Team. **NOTE: a known bug of gtselect is that if you choose one of RA, Dec or radius as INDEF, also the other two will be left as INDEF. See the end of the README for gtselect at the link [https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/gtselect.txt](https://raw.githubusercontent.com/fermi-lat/fermitools-fhelp/master/gtselect.txt)**

You can use the `gtvcut` command to check different parameters of the input photon file:

```
[you@your_super_machine] $ gtvcut photon/L20040515476099D9076805_PH00.fits

WARNING: version mismatch between CFITSIO header (v3.43) and linked library (v3.41).

Extension name[EVENTS]
DSTYP1: TIME
DSUNI1: s
DSVAL1: TABLE
DSREF1: :GTI

GTIs: (suppressed)

DSTYP2: BIT_MASK(EVENT_CLASS,128,P8R3)
DSUNI2: DIMENSIONLESS
DSVAL2: 1:1

DSTYP3: POS(RA,DEC)
DSUNI3: deg
DSVAL3: CIRCLE(77.3581856,5.69314823,30)

DSTYP4: TIME
DSUNI4: s
DSVAL4: 333244802:333849602

DSTYP5: ENERGY
DSUNI5: MeV
DSVAL5: 100:300000
```

After the selection in time, energy and position, we want to select from the spacecraft file the so called *Good Time Intervals*, that is the time intervals in which Fermi did not have any problems. To do that, we use `gtmktime`:

```
[you@your_super_machine] $ gtmktime

WARNING: version mismatch between CFITSIO header (v3.43) and linked library (v3.41).

Spacecraft data file[] ./spacecraft/L20040515476099D9076805_SC00.fits
Filter expression[DATA_QUAL>0 && LAT_CONFIG==1 && ABS(ROCK_ANGLE)<52] (DATA_QUAL>0)&&(LAT_CONFIG==1)
Apply ROI-based zenith angle cut[yes] no
Event data file[] TXS0506+056_select.fits
Output event file name[] TXS0506+056_select_gti.fits
```

Here the *filter expression* is the one recommended by the LAT team.

Before going on, we check the selection we made with `gtvcut`:

```
[you@your_super_machine] $ gtvcut TXS0506+056_select_gti.fits

WARNING: version mismatch between CFITSIO header (v3.43) and linked library (v3.41).

Extension name[EVENTS]
DSTYP1: BIT_MASK(EVENT_CLASS,128,P8R3)
DSUNI1: DIMENSIONLESS
DSVAL1: 1:1

DSTYP2: POS(RA,DEC)
DSUNI2: deg
DSVAL2: CIRCLE(77.3582,5.69315,15)

DSTYP3: TIME
DSUNI3: s
DSVAL3: TABLE
DSREF3: :GTI

GTIs: (suppressed)

DSTYP4: BIT_MASK(EVENT_TYPE,3,P8R3)
DSUNI4: DIMENSIONLESS
DSVAL4: 1:1

DSTYP5: ENERGY
DSUNI5: MeV
DSVAL5: 100:300000

DSTYP6: ZENITH_ANGLE
DSUNI6: deg
DSVAL6: 0:90
```

The several parameters are described by DSS keys which you can see from `gtvcut` output. For example, the first DSS key describes the event class we selected, the second the position and radius region and so on. <br>
**NOTE: check that there are not two DSS keys with the same name (TYP): it means that you have two cuts on that parameter. If you go on with the analysis, you will get a crash. So, go back and run again gtselect and gtmktime properly!**

## Count map ##

When you have the selected data, you can create a count map (integrate over all the energy range) with `gtbin`:

```
[you@your_super_machine] $ gtbin

This is gtbin version HEAD
Type of output file (CCUBE|CMAP|LC|PHA1|PHA2|HEALPIX) [PHA2] CMAP
Event data file name[] TXS0506+056_select_gti.fits
Output file name[] TXS0506+056_cmap.fits
Spacecraft data file name[NONE] ./spacecraft/L20040515476099D9076805_SC00.fits
Size of the X axis in pixels[] 300
Size of the Y axis in pixels[] 300
Image scale (in degrees/pixel)[] 0.1
Coordinate system (CEL - celestial, GAL -galactic) (CEL|GAL) [CEL]
First coordinate of image center in degrees (RA or galactic l)[] 77.3582
Second coordinate of image center in degrees (DEC or galactic b)[] 5.69315
Rotation angle of image axis, in degrees[0.]
Projection method e.g. AIT|ARC|CAR|GLS|MER|NCP|SIN|STG|TAN:[AIT]
```

You can see the output fits file with `ds9`.

![TXS 0506+056 count map](./likelihood_results/images/TXS0506+056_cmap.png)
<img src="./likelihood_results/images/TXS0506+056_cmap_zoom.png" width="40%">

## Exposure map calculation ##

As seen in the theoretical introduction to the likelihood analysis, the LAT Instrument Response Functions depends on the inclination angle (angle between the direction to a source and the LAT normal). Therefore the number of counts that can be detected from a source by the LAT depends on the amount of time the LAT spent at a given inclination angle during an observation. This quantity depends only on the LAT orientation history and not on the source model. In the likelihood analysis, this quantity is an array calculated for each point of the sky and called `livetime cube`.

The livetime cube is produced with `gtltcube` in bins of energy and inclination angle:

```
[you@your_super_machine] $ gtltcube zmax=90

Event data file[] TXS0506+056_select_gti.fits
Spacecraft data file[] ./spacecraft/L20040515476099D9076805_SC00.fits
Output file[expCube.fits] TXS0506+056_ltcube.fits
Step size in cos(theta) (0.:1.) [0.025]
Pixel size (degrees)[1]
Working on file ./spacecraft/L20040515476099D9076805_SC00.fits
.....................!
```

The livetime cube is used to compute the exposure map. In LAT analysis, the exposure is not the simple integral of effective area over time, but is more complex. It is defined as the integral of the complete LAT response (effective area times energy dispersion times point spread function) over the entire ROI.

We can compute the exposure map (takes time) with `gtexpmap`:

```
[you@your_super_machine] $ gtexpmap

The exposure maps generated by this tool are meant
to be used for *unbinned* likelihood analysis only.
Do not use them for binned analyses.
Event data file[] TXS0506+056_select_gti.fits
Spacecraft data file[] ./spacecraft/L20040515476099D9076805_SC00.fits
Exposure hypercube file[] TXS0506+056_ltcube.fits
output file name[] TXS0506+056_expmap.fits
Response functions[CALDB] P8R3_SOURCE_V2
Radius of the source region (in degrees)[30]
Number of longitude points (2:1000) [120]
Number of latitude points (2:1000) [120]
Number of energies (2:100) [20]
Computing the ExposureMap using TXS0506+056_ltcube.fits
....................!
```

To be noted that we specified the IRF type to P8R3\_SOURCE\_V2, which is the correct one for the source event class and type.

You can inspect the exposure in the energy bins with `ds9`.

## Model creation ##

To perform the likelihood analysis, we need a source model. The model describes the spectra of all the sources in the so called source region. One will be of course the source we are analyzing, while the others will be sources contributing to the expected number of counts. Of course, we will take into account of the galactic diffuse emission and isotropic emission. The spectra and parameters of the sources will be taken from the 4FGL catalog.

In Fermi-LAT analysis the source model is defined by an XML file, a markup language which is really useful to store and transferring data in an hardware independent way. XML uses tags, like HTML, but they are not predefined: the author of the XML can decide which are the tags to represent a given data structure and use those to create an XML file. Also, if the so-called *XML schema* is provided, a XML file can be validated against the schema to check if it is well written. Finally, XML files have the advantage to be both machine and human readable.

For each source in the source region, the source model file specifies:
* the type of source, either `PointSource` or `DiffuseSource`
* each type of source is comprised of a spectral and a spatial model component, each with several parameters.

An example of source definition is the following, a point source with power law spectrum:

```xml
<source name="PowerLaw_source" type="PointSource">
<!-- point source units are cm^-2 s^-1 MeV^-1 -->
<spectrum type="PowerLaw">
<parameter free="1" max="1000.0" min="0.001" name="Prefactor" scale="1e-09" value="1"/>
<parameter free="1" max="-1.0" min="-5." name="Index" scale="1.0" value="-2.1"/>
<parameter free="0" max="2000.0" min="30.0" name="Scale" scale="1.0" value="100.0"/>
</spectrum>
<spatialModel type="SkyDirFunction">
<parameter free="0" max="360." min="-360." name="RA" scale="1.0" value="83.45"/>
<parameter free="0" max="90." min="-90." name="DEC" scale="1.0" value="21.72"/>
</spatialModel>
</source>
```

In this case, the spectrum has 3 parameters. Each parameter has some attributes:

* `free` tells if the parameters will be left free or fixed in the likelihood maximization
* `min` and `max` are the minimum and maximum values for that parameter
* `name` is the name of the parameter
* `scale` is a multiplicative factor to `value`, which give the actual value of the parameter
* `value` is the value of the parameter, to be multiplied by `scale`

Given that the source is point-like, the spatial model just specifies the right ascension and declination of the source.

More spectral and spatial models, with XML examples, can be found in the links [https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/source\_models.html](https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/source\_models.html) and [https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/xml\_model\_defs.html](https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/xml\_model\_defs.html).

We now proceed to create the source model. We first need to download some files:

- the python script to produce the input model for our likelihood (see [https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/](https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/)):
```
[you@your_super_machine] $ wget https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/make4FGLxml.py
```

- the FITS file containing the sources in the 4FGL catalog (see [https://fermi.gsfc.nasa.gov/ssc/data/access/lat/8yr_catalog/](https://fermi.gsfc.nasa.gov/ssc/data/access/lat/8yr_catalog/))
```
[you@your_super_machine] $ wget https://fermi.gsfc.nasa.gov/ssc/data/access/lat/8yr_catalog/gll_psc_v21.fit
```

We need also the files containing the galactic diffuse background modeled emission and the isotropic background emission, but they are shipped together with the Fermitools. They can be found in the directory `$(FERMI_DIR)/refdata/fermi/galdiffuse/`. To choose the correct background models, we refer to the table in [https://fermi.gsfc.nasa.gov/ssc/data/access/lat/BackgroundModels.html](https://fermi.gsfc.nasa.gov/ssc/data/access/lat/BackgroundModels.html).

We are now ready to create our model. There is a user contributed tool called `make4FGLxml.py` which can be used to do that. You can find out all its options and command line arguments with `python make4FGLxml.py -h` or at the link [https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/readme_make4FGLxml.txt](https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/readme_make4FGLxml.txt).

Here below you find an example applied to our TXS 0506+056 case:

```
[you@your_super_machine] $ python make4FGLxml.py gll_psc_v21.fit TXS0506+056_select_gti.fits -G 
/storage/gpfs_data/ctalocal/aberti/miniconda2/envs/fermi/share/fermitools/refdata/fermi/galdiffuse/gll_iem_v07.fits -g gll_iem_v07 
-I /storage/gpfs_data/ctalocal/aberti/miniconda2/envs/fermi/share/fermitools/refdata/fermi/galdiffuse/iso_P8R3_SOURCE_V2_v1.txt -i iso_P8R3_SOURCE_V2_v1 
-r 0.01 -o TXS0506+056_input_model.xml

This is make4FGLxml version 01r05.
The default diffuse model files and names are for pass 8 and 4FGL and assume you have v11r5p3 of the Fermi Science Tools or higher.
Creating file and adding sources from 4FGL
Extended source S 147 in ROI, make sure $(FERMI_DIR)/data/pyBurstAnalysisGUI/templates/S147.fits is the correct path to the extended template.
Extended source IC 443 in ROI with RadialGaussian spatial model.
Extended source Rosette in ROI, make sure $(FERMI_DIR)/data/pyBurstAnalysisGUI/templates/Rosette.fits is the correct path to the extended template.
Extended source Monoceros in ROI with RadialGaussian spatial model.
Added 164 point sources and 4 extended sources
If using unbinned likelihood you will need to rerun gtdiffrsp for the extended sources or rerun the makeModel function with optional argument psForce=True
```

A bit of explanation:

- the first argument is the FITS file containing the 4FGL catalog
- the second argument is the event file
- **-o** specifies the name of the output model file
- **-G** specifies the location of the galactic diffuse emission FITS file
- **-g** specifies the name of the galactic diffuse emission in the output model
- **-I** specifies the location of the isotropic emission file
- **-i** specifies the name of the isotropic emission in the output model
- **-r** specifies the radius beyond which the parameters of the source will be fixed
- by default, the source region is defined to be 10deg more in radius with respect the ROI defined in the event file (so in our case, the source region is 25deg in radius). This parameter can be set with the option `-ER`.

By specifying `-r 0.01` we want to keep fixed the parameters of all sources, except the one of our interest, TXS 0506+056. In the model file, our source will appear with its 4FGL name, that is 4FGL J0509.4+0542, and with a spectral model which is again the one from the 4FGL. In our case we see this:

```xml
<source ROI_Center_Distance="0.008" name="4FGL J0509.4+0542" type="PointSource">
        <spectrum type="LogParabola">
        <!-- Source is 0.008302947268499733 degrees away from ROI center -->
        <!-- Source is outside specified radius limit of 0.001 -->
                <parameter free="0" max="1e4" min="1e-4" name="norm" scale="1e-12" value="5.061729247551883"/>
                <parameter free="0" max="5.0" min="0.0" name="alpha" scale="1.0" value="1.9898329"/>
                <parameter free="0" max="10.0" min="0.0" name="beta" scale="1.0" value="0.0512963"/>
                <parameter free="0" max="5e5" min="30" name="Eb" scale="1.0" value="1126.4865"/>
        </spectrum>
        <spatialModel type="SkyDirFunction">
                <parameter free="0" max="360.0" min="-360.0" name="RA" scale="1.0" value="77.3593"/>
                <parameter free="0" max="90" min="-90" name="DEC" scale="1.0" value="5.7014"/>
        </spatialModel>
</source>
```

We can see that, as we wanted, the spectral parameters of the source are left free. Also, the 4FGL spectrum for TXS 0506+056 is described by a log parabola, instead of a power law. You can see the definition of the log parabola spectrum [here](https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/source_models.html#LogParabola).

We now quickly inspect the rest of the model file. Looking at the output of `make4FGLxml.py`, we see that 4 extended sources were added: IC 443, S147, Monoceros and Rosette. Of course, as any other source, these extended sources are added in the XML file with their spectral and spatial model. For extended sources, the spatial model can be specified in two ways:

1. via a `SkyDirFunction` (for point sources or radial models) or `ConstantValue`
2. via a `SpatialMap ` or `MapCubeFunction`, both needing a FITS file

Looking again at the output of `make4FGLxml.py`, we see that for example Monoceros was added with a `RadialGaussian` spatial model, belonging to the first category:

```xml
<source ROI_Center_Distance="22.397" name="Monoceros" type="DiffuseSource">
        <spectrum type="LogParabola">
        <!-- Source is 22.39737384822301 degrees away from ROI center -->
        <!-- Source is outside ROI, all parameters should remain fixed -->
                <parameter free="0" max="1e4" min="1e-4" name="norm" scale="1e-11" value="3.119163954901083"/>
                <parameter free="0" max="5.0" min="0.0" name="alpha" scale="1.0" value="2.1040795"/>
                <parameter free="0" max="10.0" min="0.0" name="beta" scale="1.0" value="0.2782915"/>
                <parameter free="0" max="5e5" min="30" name="Eb" scale="1.0" value="755.66"/>
        </spectrum>
        <spatialModel type="RadialGaussian">
        <parameter free="0" max="360" min="-360" name="RA" scale="1" value="99.86"/>
        <parameter free="0" max="90" min="-90" name="DEC" scale="1" value="6.93"/>
        <parameter free="0" max="10" min="0" name="Sigma" scale="1" value="3.47"/>
        </spatialModel>
</source>
```

S147 instead was added with a `SpatialMap` model:

```xml
<source ROI_Center_Distance="23.430" name="S 147" type="DiffuseSource">
        <spectrum type="LogParabola">
        <!-- Source is 23.4296966000806 degrees away from ROI center -->
        <!-- Source is outside ROI, all parameters should remain fixed -->
                <parameter free="0" max="1e4" min="1e-4" name="norm" scale="1e-12" value="6.7504717093935795"/>
                <parameter free="0" max="5.0" min="0.0" name="alpha" scale="1.0" value="2.0195098"/>
                <parameter free="0" max="10.0" min="0.0" name="beta" scale="1.0" value="0.124881044"/>
                <parameter free="0" max="5e5" min="30" name="Eb" scale="1.0" value="1107.1104"/>
        </spectrum>
        <spatialModel file="$(FERMI_DIR)/data/pyBurstAnalysisGUI/templates/S147.fits" map_based_integral="true" type="SpatialMap">
                <parameter free="0" max="1000" min="0.001" name="Prefactor" scale="1" value="1"/>
        </spatialModel>
</source>
```

Therefore, a FITS file for S147 and for Rosette must be given in order to describe those two extended sources. The FITS files in this case contain a template for determining the distribution of photons on the sky. The extended sources templates can be found in the page [https://fermi.gsfc.nasa.gov/ssc/data/access/lat/8yr_catalog/](https://fermi.gsfc.nasa.gov/ssc/data/access/lat/8yr_catalog/), see *Extended Source template archive*.

Let's download it:

```
[you@your_super_machine] $ wget https://fermi.gsfc.nasa.gov/ssc/data/access/lat/8yr_catalog/LAT_extended_sources_8years.tgz
--2020-04-06 08:57:24--  https://fermi.gsfc.nasa.gov/ssc/data/access/lat/8yr_catalog/LAT_extended_sources_8years.tgz
Resolving fermi.gsfc.nasa.gov (fermi.gsfc.nasa.gov)... 129.164.179.26, 2001:4d0:2310:150::26
Connecting to fermi.gsfc.nasa.gov (fermi.gsfc.nasa.gov)|129.164.179.26|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 7306126 (7.0M) [application/x-tar]
Saving to: ‘LAT_extended_sources_8years.tgz’

100%[========================================================================================================>] 7,306,126    523KB/s   in 15s

2020-04-06 08:57:39 (484 KB/s) - ‘LAT_extended_sources_8years.tgz’ saved [7306126/7306126]

[you@your_super_machine] $ tar -xzf LAT_extended_sources_8years.tgz
```

The last command will create some new directories. One of these, `Templates`, contains the FITS files needed for the extended sources in our source model file:

```
[you@your_super_machine] $ cd Templates

[you@your_super_machine] $ ls
CenALobes.fits   HB9.fits            LMC-FarWest.fits  MSH15-56_PWN.fits      Rosette.fits              SMC-Galaxy.fits  W51C.fits
CygnusLoop.fits  HESSJ1841-055.fits  LMC-Galaxy.fits   MSH15-56_SNRmask.fits  RXJ1713_2016_250GeV.fits  W3.fits
FornaxA.fits     LMC-30DorWest.fits  LMC-North.fits    RCW86.fits             S147.fits                 W44.fits
```

Therefore, for S147 and Rosette we can replace `$(FERMI_DIR)/data/pyBurstAnalysisGUI/templates/S147.fits` and `$(FERMI_DIR)/data/pyBurstAnalysisGUI/templates/Rosette.fits` with `./Templates/S147.fits` and `./Templates/Rosette.fits` respectively.

Before going on with the next step, we take a look at the diffuse galactic and isotropic extragalactic models in the XML:

```xml
<!-- Diffuse Sources -->
<source name="gll_iem_v07" type="DiffuseSource">
        <spectrum type="PowerLaw">
                <parameter free="1" max="10" min="0" name="Prefactor" scale="1" value="1"/>
                <parameter free="0" max="1" min="-1" name="Index" scale="1.0" value="0"/>
                <parameter free="0" max="2e2" min="5e1" name="Scale" scale="1.0" value="1e2"/>
        </spectrum>
        <spatialModel file="/storage/gpfs_data/ctalocal/aberti/miniconda2/envs/fermi/share/fermitools/refdata/fermi/galdiffuse/gll_iem_v07.fits" type="MapCubeFunction">
                <parameter free="0" max="1e3" min="1e-3" name="Normalization" scale="1.0" value="1.0"/>
        </spatialModel>
</source>
<source name="iso_P8R3_SOURCE_V2_v1" type="DiffuseSource">
        <spectrum apply_edisp="false" file="/storage/gpfs_data/ctalocal/aberti/miniconda2/envs/fermi/share/fermitools/refdata/fermi/galdiffuse/iso_P8R3_SOURCE_V2_v1.txt" type="FileFunction">
                <parameter free="1" max="10" min="1e-2" name="Normalization" scale="1" value="1"/>
        </spectrum>
        <spatialModel type="ConstantValue">
                <parameter free="0" max="10.0" min="0.0" name="Value" scale="1.0" value="1.0"/>
        </spatialModel>
</source>
```

You can see that here the paths to the FITS and txt files are already globbed, pointing to the right directory, as specified to the `make4FGLxml.py` script. Also, we see that they have some free parameters and we will leave them like this, so that they will enter in the maximization process.

## Diffuse sources responses computation ##

The likelihood function used in the maximization process has to take into account the contribution of each source in the model. The contribution to the log-likelihood associated with an individual photon is computed as the integral of the source model with the instrument response evaluated at the observed photon direction, energy and arrival time. For point sources, the spatial component is a delta-function, so this integral is relatively easy to do. However, for diffuse sources such as the Galactic interstellar component, this integral is very computationally intensive since it must be performed over the whole sky (in principle). Therefore, if possible, it is useful to precompute these quantities in advance. In the likelihood calculations, it is assumed that the spatial and spectral parts of a source model factor in such a way that the integral over spatial distribution of a source can be performed independently of the spectral part and in this case the integral over the instrument response can be precomputed for each diffuse model component.

Since in our model we have diffuse sources, we should precompute the so-called *diffuse responses*. If we do not this now, it will be done during the maximization process. But since we plan to run the likelihood maximization more than once, it is better to precompute the diffuse responses.

The tool to do this is `gtdiffrsp`:

```
[you@your_super_machine] $ gtdiffrsp evfile=TXS0506+056_select_gti.fits scfile=./spacecraft/L20040515476099D9076805_SC00.fits 
srcmdl=TXS0506+056_input_model.xml irfs=P8R3_SOURCE_V2
```

In this case we used the command line version of the command, where the arguments should be quite self-explanatory. This step can take some time (around 30 minutes in this case).

## Likelihood maximization ##

We have now everything ready to run the likelihood maximization. The goal is to finding the set of parameters that maximizes the likelihood, so the parameters that best represent our data given the input source model. The maximum is found by iteration, where the likelihood function is computed each time with a different set of trial parameters. The derivatives of the function with respect to the parameters are used to set new trial parameters for the next iteration, approaching values maximizing the function. This iterative process stops either when the change in the function is sufficiently small or when the number of iteration reaches a predefined maximum value.

The tool used in LAT to maximize the likelihood is `gtlike`, which uses different algorithms, called optimizers, to maximize the likelihood. The different optimizers differ in how rapidly they converge to the maximum, the amount of memory used and the accuracy in estimating the uncertainties on the parameters.

Let's run `gtlike`:

```
[you@your_super_machine] $ gtlike refit=yes plot=yes sfile=TXS0506+056_output_model.xml

Statistic to use (BINNED|UNBINNED) [UNBINNED]
Spacecraft file[./spacecraft/L20040515476099D9076805_SC00.fits]
Event file[TXS0506+056_select_gti.fits]
Unbinned exposure map[TXS0506+056_expmap.fits]
Exposure hypercube file[TXS0506+056_ltcube.fits]
Source model file[TXS0506+056_input_model.xml]
Response functions to use[P8R3_SOURCE_V2]
Optimizer (DRMNFB|NEWMINUIT|MINUIT|DRMNGB|LBFGS) [NEWMINUIT]
```

The output of `gtlike` is quite long, since it will write all the values of all the parameters of all the sources in the input file. The source model with the parameter values found after the maximization are put in the output model (TXS0506+056\_output\_model.xml). Moreover, since we enabled the option `refit`, after the first fit, we can fit again starting with the values found in the first maximization to try to have better estimates of the values. <br>
Here below you can find part of the output of `gtlike`:

```
TXS 0506+056:
norm: 20.3368 +/- 4.95187
alpha: 2.05487 +/- 0.170727
beta: 0.109391 +/- 0.116849
Eb: 1126.49
Npred: 78.9983
ROI distance: 0.00832229
TS value: 106.864
Flux: 2.04313e-07 +/- 5.77649e-08 photons/cm^2/s

gll_iem_v07:
Prefactor: 0.939001 +/- 0.0351567
Index: 0
Scale: 100
Npred: 3260.18
Flux: 0.000489152 +/- 1.83114e-05 photons/cm^2/s

iso_P8R3_SOURCE_V2_v1:
Normalization: 1.06814 +/- 0.160173
Npred: 776.309
Flux: 0.000131239 +/- 1.96756e-05 photons/cm^2/s

WARNING: Fit may be bad in range [2459.51, 3670.33] (MeV)

Total number of observed counts: 4638
Total number of model events: 4633.08

-log(Likelihood): 47288.11513

Writing fitted model to TXS0506+056_output_model.xml
```

As you can see, the output of `gtlike` shows the value of the parameters for our source, TXS 0506+056, together with their uncertainties. Moreover, we get a value for the TS (106, corresponding to \~10 sigma) and for the source flux.

You can find the summary of all of the sources and their final parameters in the file `result.dat`.

Since we enabled the `plot` option of `gtlike`, we have the plot with the spectra for all sources (the summed model) and for each source independently. The second plot shows the residuals of the summed model fit.

The color codes are:

* Black - summed model
* Red - first source in results.dat file
* Green - second source
* Blue - third source
* Magenta - fourth source
* Cyan - the fifth source
* repetition of the colors starting from red

## TS map ##

Finally, it is possible that in our model we missed some sources which are not in the 4FGL catalog. To search for unmodeled point sources, we create the so called *Test-statistic map* or more simply, *TS map*. A TS map is created by moving a putative point source through a grid of locations on the source region and maximizing -log(likelihood) at each point, with the other sources included in the fit. If other fainter sources are identified, then they are found in the grid point where we have local maxima of the TS.

The tool to create TS maps is called `gttsmap` and we will give as input the output model from gtlike. We can actually create two maps:

* a *residual map*, which can be created using the output model from `gtlike` but fixing all parameters of the model for each source (let's call it `TXS0506+056_tsmap_residual.xml`)

* a *source map*, using the same model for the residual map, but the source of interest is removed from the model (let's call it `TXS0506+056_tsmap_source.xml`).

The creation of the TS map requires a lot of time, since many likelihood maximizations need to be performed. So we run `gttsmap` in the command line form and run it in the background:

```
[you@your_super_machine] $ gttsmap statistic=UNBINNED evfile=TXS0506+056_select_gti.fits 
scfile=./spacecraft/L20040515476099D9076805_SC00.fits expmap=TXS0506+056_expmap.fits 
expcube=TXS0506+056_ltcube.fits srcmdl=TXS0506+056_tsmap_residual.xml 
outfile=TXS0506+056_tsmap_residual.fits irfs=P8R3_SOURCE_V2 optimizer=NEWMINUIT ftol=1e-05 
nxpix=40 nypix=40 binsz=0.5 xref=77.3582 yref=5.69315 coordsys=CEL proj=AIT &

[you@your_super_machine] $ gttsmap statistic=UNBINNED evfile=TXS0506+056_select_gti.fits 
scfile=./spacecraft/L20040515476099D9076805_SC00.fits expmap=TXS0506+056_expmap.fits 
expcube=TXS0506+056_ltcube.fits srcmdl=TXS0506+056_tsmap_source.xml 
outfile=TXS0506+056_tsmap_source.fits  irfs=P8R3_SOURCE_V2 optimizer=NEWMINUIT ftol=1e-05 
nxpix=40 nypix=40 binsz=0.5 xref=77.3582 yref=5.69315 coordsys=CEL proj=AIT &
```

where `srcmdl` is the file model used in the creation of the TS map, `ftol` specifies the value of the relative tolerance in the likelihood fit, while `nxpix`, `nypix` and `binsz` specify the number and size of grid point. Therefore, the TS map will have 1600 pixels i.e. 1600 likelihood maximizations. A lot.

Probably, between the two TS maps that can be created depending if the source is included or not in the model used by `gttsmap`, the *residual map* is the most interesting, since it will show if there are fainter sources in the sky region which were not taken into account in the model. Therefore, if such a source is found, the likelihood analysis can be repeated taking into account this new source in the model, so that it will describe data better, to be compared with the previous model.

In the image here below, on the left there is the residual TS map. We see that it is pretty flat in TS space, meaning that no significant sources were left out from the model. On the right instead there is the source TS map. Here the most prominent source is TXS 0506+056 in the center.

<img src="./likelihood_results/images/TXS0506+056_tsmaps.png" width="70%">

## Light curves ##

Creating light curves using the likelihood analysis is quite straightforward. Since among the outputs of the likelihood analysis we have the flux (and its error) of our source in a given period of time (one week in our case), a light curve can be produced by running several likelihood analysis over different periods of time and taking the resulting flux values.

For example, given our dataset, one can try to select the data day by day and run for each a likelihood analysis. This will result in a daily light curve for our source.

Most probably, the better way to do this, since it is easier to customize, is to use the python wrappers of the Fermitools, described in the [python likelihood tutorial](https://gitlab.com/aleberti/fermitools_tutorial/-/blob/master/likelihood_tutorial_python.md).