# Likelihood tutorial with Python #

Now that we finished the likelihood, we can try to do the same analysis with the Python wrappers of the Fermi Tools: [https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/python_tutorial.html](https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/python_tutorial.html). Wrapper just means the following thing: you will use python only to set up the settings for the several executables we used before (`gtselect`, `gtmktime`, `gtltcube` and so on), but at the end the same executables will be called under the hood. So it's not like the Fermi Tools were re-written in Python.

## Data set ##

As data set we will use the same one used in the unbinned likelihood tutorial. Here below you find a summary of the data set as given from the query in the LAT data server:

* Object name or coordinates: TXS0506+056
* Coordinate system: J2000
* Search radius (degrees): 30
* Observation dates: 2011-07-25 00:00:00, 2011-08-01 00:00:00
* Time system: Gregorian
* Energy range (MeV): 100, 300000
* LAT data type: Photon
* Spacecraft data: Checked

Again, assuming you already have the data, create a folder inside the directory `TXS0506+056` where you will store the outputs of the analysis performed with python:

```
[you@your_super_machine] $ cd TXS0506+056

[you@your_super_machine] $ mkdir python_analysis

[you@your_super_machine] $ cp -r photon/ spacecraft/ python_analysis/

[you@your_super_machine] $ cd python_analysis

[you@your_super_machine] $ conda activate fermi
```

Therefore, we have copied the data used in the unbinned likelihood tutorial inside the `python_analysis` directory and setup the Fermitools. We are now ready to start the analysis with Python.

## Data selection ##

We proceed with the selection of the data. As before, we want to select the data according to the following criteria:

* the center of the ROI is TXS 0506+056
* reduce the size of the ROI from 30deg to 15deg of radius
* select events of the `SOURCE` class and event types `Front+Back`
* the time interval is the same as the one used in the data query
* energy range between 100 MeV and 300 GeV

At this point we can dig into python (`>>>` is the python interpreter prompt):

```python
[you@your_super_machine] $ python
Python 2.7.14 | packaged by conda-forge | (default, Oct  5 2017, 14:19:56)
[GCC 4.8.2 20140120 (Red Hat 4.8.2-15)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Now, to use the python wrappers of the Fermi tools, you need to import the module named `gt_apps`. We will import it with the name `my_apps`:

```python
>>> import gt_apps as my_apps
```

For almost all the Fermi tools executables like `gtselect`, `gtmktime`, `gtexpmap` and so on, the module `gt_apps` defines a different python object whose attributes are the same as the
command line arguments of the executables.

For example, the corresponding python object for `gtselect` is called `filter`. The attributes are set through python dictionaries (key, value pairs) like shown below:

```python
>>> my_apps.filter['evclass'] = 128
>>> my_apps.filter['evtype']  = 3
>>> my_apps.filter['ra']      = 77.3582
>>> my_apps.filter['dec']     = 5.69315
>>> my_apps.filter['rad']     = 15
>>> my_apps.filter['emin']    = 100
>>> my_apps.filter['emax']    = 300000
>>> my_apps.filter['zmax']    = 90
>>> my_apps.filter['tmin']    = 333244802
>>> my_apps.filter['tmax']    = 333849602
>>> my_apps.filter['infile']  = './photon/L20040515476099D9076805_PH00.fits'
>>> my_apps.filter['outfile'] = 'TXS0506+056_select.fits'
```

The meaning of the different keys is pretty self-explanatory. If you need help or documentation, just do:

```python
>>> help(my_apps)
```

You can check the command that will be run with the `command()` method:

```python
>>> print(my_apps.filter.command())

time -p gtselect infile=./photon/L20040515476099D9076805_PH00.fits outfile=TXS0506+056_select.fits
ra=77.3582 dec=5.69315 rad=15.0 tmin=333244802.0 tmax=333849602.0 emin=100.0 emax=300000.0 zmin=0.0
zmax=90.0 evclass=128 evtype=3 convtype=-1 phasemin=0.0 phasemax=1.0 evtable="EVENTS" chatter=2
clobber=yes debug=no gui=no mode="ql"
```

To run `gtselect` through the python wrapper `filter`, just use the `run()` method:

```python
>>> my_apps.filter.run()

time -p gtselect infile=./photon/L20040515476099D9076805_PH00.fits outfile=TXS0506+056_select.fits 
ra=77.3582 dec=5.69315 rad=15.0 tmin=333244802.0 tmax=333849602.0 emin=100.0 emax=300000.0 zmin=0.0
zmax=90.0 evclass=128 evtype=3 convtype=-1 phasemin=0.0 phasemax=1.0 evtable="EVENTS" chatter=2 
clobber=yes debug=no gui=no mode="ql"

Done.
real 3.49
user 1.85
sys 2.04
```

When calling `run()`, the terminal will show you the command called with the Fermi Tools, in this case `gtselect`, with all the proper command line arguments. You can see that there are some
command line arguments whose values we didn't set through the `filter` attributes. In those cases, the default values are taken e.g. phasemin and phasemax are 0 and 1 respectively i.e. no cut in phase is done.
If we would like to put a cut in the phase, we could do it setting the value for `phasemin` and `phasemax` like we did for the other attributes.

Instead of typing every time on the python interpreter, let's write some scripts. For example, let's call `pygtselect.py` the python script which is the equivalent of the commands we just gave on the python interpreter. Its content will be simply a copy and paste of those commands:

```python
import gt_apps as my_apps

my_apps.filter['evclass'] = 128
my_apps.filter['evtype']  = 3
my_apps.filter['ra']      = 77.3582
my_apps.filter['dec']     = 5.69315
my_apps.filter['rad']     = 15
my_apps.filter['emin']    = 100
my_apps.filter['emax']    = 300000
my_apps.filter['zmax']    = 90
my_apps.filter['tmin']    = 333244802
my_apps.filter['tmax']    = 333849602
my_apps.filter['infile']  = './photon/L20040515476099D9076805_PH00.fits'
my_apps.filter['outfile'] = 'TXS0506+056_select.fits'

print(my_apps.filter.command())

my_apps.filter.run()
```

We can run this script very simply:

```
[you@your_super_machine] $ python pygtselect.py
time -p gtselect infile=./photon/L20040515476099D9076805_PH00.fits outfile=TXS0506+056_select.fits 
ra=77.3582 dec=5.69315 rad=15.0 tmin=333244802.0 tmax=333849602.0 emin=100.0 emax=300000.0 zmin=0.0 
zmax=90.0 evclass=128 evtype=3 convtype=-1 phasemin=0.0 phasemax=1.0 evtable="EVENTS" chatter=2 
clobber=yes debug=no gui=no mode="ql"

Done.
real 3.49
user 1.85
sys 2.04
```

The philosophy shown for `gtselect` works for all the other executables, as long as we know the name of the corresponding python object.

For example, the python object corresponding to `gtmktime` is `maketime`. We create another script, `pygtmktime.py`, with the settings for quality selection:

```python
import gt_apps as my_apps

my_apps.maketime['scfile']  = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.maketime['filter']  = '(DATA_QUAL>0)&&(LAT_CONFIG==1)'
my_apps.maketime['roicut']  = 'no'
my_apps.maketime['evfile']  = 'TXS0506+056_select.fits'
my_apps.maketime['outfile'] = 'TXS0506+056_select_gti.fits'

print(my_apps.maketime.command())

my_apps.maketime.run()
```

and run it:

```
[you@your_super_machine] $ python pygtmktime.py

time -p gtmktime scfile=./spacecraft/L20040515476099D9076805_SC00.fits sctable="SC_DATA" 
filter="(DATA_QUAL>0)&&(LAT_CONFIG==1)" roicut=no evfile=TXS0506+056_select.fits evtable="EVENTS" 
outfile="TXS0506+056_select_gti.fits" apply_filter=yes overwrite=no header_obstimes=yes tstart=0.0 
tstop=0.0 gtifile="default" chatter=2 clobber=yes debug=no gui=no mode="ql"

real 0.56
user 0.78
sys 1.80
```

With this, we have our data selection. Before proceeding, let's try to set up in a pythonic way one of the executables which does not have a corresponding python object. This is the case of `gtvcut`. We can follow the recommendations in the LAT analysis thread to see how to do it:

```python
>>> from GtApp import GtApp
>>> vcut = GtApp('gtvcut', 'Likelihood')
>>> vcut['infile'] = 'TXS0506+056_select_gti.fits'
>>> vcut.command()
'time -p gtvcut infile=TXS0506+056_select_gti.fits table="EVENTS" suppress_gtis=yes chatter=2 debug=no gui=no mode="ql"'
>>> vcut.run()

time -p gtvcut infile=TXS0506+056_select_gti.fits table="EVENTS" suppress_gtis=yes chatter=2 debug=no gui=no mode="ql"

WARNING: version mismatch between CFITSIO header (v3.43) and linked library (v3.41).

DSTYP1: BIT_MASK(EVENT_CLASS,128,P8R3)
DSUNI1: DIMENSIONLESS
DSVAL1: 1:1

DSTYP2: POS(RA,DEC)
DSUNI2: deg
DSVAL2: CIRCLE(77.3582,5.69315,15)

DSTYP3: TIME
DSUNI3: s
DSVAL3: TABLE
DSREF3: :GTI

GTIs: (suppressed)

DSTYP4: BIT_MASK(EVENT_TYPE,3,P8R3)
DSUNI4: DIMENSIONLESS
DSVAL4: 1:1

DSTYP5: ENERGY
DSUNI5: MeV
DSVAL5: 100:300000

DSTYP6: ZENITH_ANGLE
DSUNI6: deg
DSVAL6: 0:90

real 8.55
user 0.72
sys 1.83
```

## Count map ##

For the creation of the count map, we need the corresponding python object for `gtbin`, which is called `evtbin`. Let's create a script called `pygtbin.py`:

```python
import gt_apps as my_apps

my_apps.evtbin['algorithm'] = 'CMAP'
my_apps.evtbin['emin']      = 100
my_apps.evtbin['emax']      = 300000
my_apps.evtbin['evfile']    = 'TXS0506+056_select_gti.fits'
my_apps.evtbin['scfile']    = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.evtbin['outfile']   = 'TXS0506+056_cmap.fits'
my_apps.evtbin['nxpix']     = 300
my_apps.evtbin['nypix']     = 300
my_apps.evtbin['binsz']     = 0.1
my_apps.evtbin['coordsys']  = 'CEL'
my_apps.evtbin['xref']      = 77.3582
my_apps.evtbin['yref']      = 5.69315
my_apps.evtbin['axisrot']   = 0
my_apps.evtbin['proj']      = 'AIT'

my_apps.evtbin.run()
```

and run it:

```
[you@your_super_machine] $ python pygtbin.py
time -p gtbin evfile=TXS0506+056_select_gti.fits scfile=./spacecraft/L20040515476099D9076805_SC00.fits outfile=TXS0506+056_cmap.fits 
algorithm="CMAP" ebinalg="LOG" emin=100.0 emax=300000.0 ebinfile=NONE tbinalg="LIN" tbinfile=NONE 
nxpix=300 nypix=300 binsz=0.1 coordsys="CEL" xref=77.3582 yref=5.69315 axisrot=0.0 rafield="RA" 
decfield="DEC" proj="AIT" hpx_ordering_scheme="RING" hpx_order=3 hpx_ebin=yes hpx_region="" 
evtable="EVENTS" sctable="SC_DATA" efield="ENERGY" tfield="TIME" chatter=2 clobber=yes debug=no 
gui=no mode="ql"

This is gtbin version HEAD
real 5.74
user 0.96
sys 1.89
```

## Exposure map calculation ##

Let's go on with `gtltcube` and `gtexpmap`, whose python objects are `expCube` and `expMap` respectively. Let's create two python scripts, called `pygtltcube.py` and `pygtexpmap.py` respectively:

```python
# pygtltcube.py
import gt_apps as my_apps

my_apps.expCube['evfile']    = 'TXS0506+056_select_gti.fits'
my_apps.expCube['scfile']    = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.expCube['outfile']   = 'TXS0506+056_ltcube.fits'
my_apps.expCube['zmax']      = 90
my_apps.expCube['dcostheta'] = 0.025
my_apps.expCube['binsz']     = 1

print(my_apps.expCube.command())

my_apps.expCube.run()
```

```python
# pygtexpmap.py

import gt_apps as my_apps

my_apps.expMap['evfile']    = 'TXS0506+056_select_gti.fits'
my_apps.expMap['scfile']    = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.expMap['expcube']   = 'TXS0506+056_ltcube.fits'
my_apps.expMap['outfile']   = 'TXS0506+056_expmap.fits'
my_apps.expMap['irfs']      = 'P8R3_SOURCE_V2'
my_apps.expMap['srcrad']    = 30
my_apps.expMap['nlong']     = 120
my_apps.expMap['nlat']      = 120
my_apps.expMap['nenergies'] = 20

print(my_apps.expMap.command())

my_apps.expMap.run()
```

Let's run them one after the other:

```
[you@your_super_machine] $ python pygtltcube.py

time -p gtltcube evfile="TXS0506+056_select_gti.fits" evtable="EVENTS" scfile=./spacecraft/L20040515476099D9076805_SC00.fits 
sctable="SC_DATA" outfile=TXS0506+056_ltcube.fits dcostheta=0.025 binsz=1.0 phibins=0 tmin=0.0 
tmax=0.0 file_version="1" zmin=0.0 zmax=90.0 chatter=2 clobber=yes debug=no gui=no mode="ql"

Working on file ./spacecraft/L20040515476099D9076805_SC00.fits
.....................!
real 47.13
user 36.32
sys 2.69

[you@your_super_machine] $ python pygtexpmap.py

time -p gtexpmap evfile=TXS0506+056_select_gti.fits evtable="EVENTS" scfile=./spacecraft/L20040515476099D9076805_SC00.fits 
sctable="SC_DATA" expcube=TXS0506+056_ltcube.fits outfile=TXS0506+056_expmap.fits irfs="P8R3_SOURCE_V2" 
evtype="INDEF" srcrad=30.0 nlong=120 nlat=120 nenergies=20 submap=no nlongmin=0 nlongmax=0 
nlatmin=0 nlatmax=0 chatter=2 clobber=yes debug=no gui=no mode="ql"

The exposure maps generated by this tool are meant
to be used for *unbinned* likelihood analysis only.
Do not use them for binned analyses.
Computing the ExposureMap using TXS0506+056_ltcube.fits
....................!
real 747.83
user 698.61
sys 43.85
```

We can now create our model.

## Model creation ##

For the model creation, we will use again the script `make4FGLxml.py`, but this time we will use it importing it as a python module. To see how we should use it, we check its documentation at the link [https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/make4FGLxml.py](https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/make4FGLxml.py). So we see that a class named `srcList` is defined, which has some members used to initialize it:

* `sources`: filename of LAT source list fits file in catalog format
* `ft1`: filename of event file for which the xml will be used, only used to extract ROI info
* `out`: name of output xml file, defaults to mymodel.xml

This class has a method called `makeModel`, which has many arguments. We list here below the ones that we need:

* `GDfile`: location and name of Galactic diffuse model to use
* `GDname`: name of Galactic diffuse component to use in xml model
* `ISOfile`: location and name of Isotropic diffuse template to use
* `ISOname`: name of Isotropic diffuse component to use in xml model
* `radLim`: radius in degrees from center of ROI beyond which source parameters are fixed

With this information at hand, we are ready to create our model. Let's create a script called `create_model.py`:

```python
from make4FGLxml import *

mymodel = srcList('gll_psc_v21.fit','TXS0506+056_select_gti.fits','TXS0506+056_input_model.xml')
mymodel.makeModel(GDfile="/storage/gpfs_data/ctalocal/aberti/miniconda2/envs/fermi/share/fermitools/refdata/fermi/galdiffuse/gll_iem_v07.fits", 
    GDname='gll_iem_v07',ISOfile="/storage/gpfs_data/ctalocal/aberti/miniconda2/envs/fermi/share/fermitools/refdata/fermi/galdiffuse/iso_P8R3_SOURCE_V2_v1.txt", 
    ISOname='iso_P8R3_SOURCE_V2_v1', radLim=0.01)
```

This is equivalent to the command we used by running `make4FGLxml.py` from the command line in the unbinned likelihood tutorial. As before, we need to set the correct path to the FITS files for the extended sources in the ROI and we set the name of our source, TXS 0506+056.

After this, we expect to have a model file identical to the one we created in the unbinned likelihood tutorial. You can check it with the `diff` command.

## Diffuse sources responses computation ##

Now it is time for the computation of the diffuse responses. The python object for `gtdiffrsp` is called `diffResps` and we set it in the script `pygtdiffrsp.py`:

```python
import gt_apps as my_apps

my_apps.diffResps['evfile'] = 'TXS0506+056_select_gti.fits'
my_apps.diffResps['scfile'] = './spacecraft/L20040515476099D9076805_SC00.fits'
my_apps.diffResps['srcmdl'] = 'TXS0506+056_input_model.xml'
my_apps.diffResps['irfs']   = 'P8R3_SOURCE_V2'

print(my_apps.diffResps.command())

my_apps.diffResps.run()
```

Let's run it:

```
[you@your_super_machine] $ python pygtdiffrsp.py

time -p gtdiffrsp evfile=TXS0506+056_select_gti.fits evtable="EVENTS" scfile=./spacecraft/L20040515476099D9076805_SC00.fits 
sctable="SC_DATA" srcmdl=TXS0506+056_input_model.xml irfs="P8R3_SOURCE_V2" evclsmin=0 evclass="INDEF" 
evtype="INDEF" convert=no chatter=2 clobber=no debug=no gui=no mode="ql"

adding source IC 443
adding source Monoceros
adding source Rosette
adding source S 147
adding source gll_iem_v07
adding source iso_P8R3_SOURCE_V2_v1
Working on...
TXS0506+056_select_gti.fits.....................!
real 2052.32
user 2032.65
sys 6.03
```

## Likelihood maximization ##

Let's go on with the likelihood within python. The Fermitools provide the `pyLikelihood` and the `UnbinnedAnalysis` modules:

```python
>>> import pyLikelihood
>>> from UnbinnedAnalysis import *
```

For more info on the `pyLikelihood` module usage, see [this page](https://fermi.gsfc.nasa.gov/ssc/data/analysis/scitools/python_usage_notes.html).

Then we create `obs`, an object of type `UnbinnedObs`, for which we have to specify (in order) the name of the event file, the spacecraft file, the exposure map
file, the livetime cube file and the IRF type:

```python
>>> obs = UnbinnedObs('TXS0506+056_select_gti.fits','./spacecraft/L20040515476099D9076805_SC00.fits', expMap='TXS0506+056_expmap.fits', expCube='TXS0506+056_ltcube.fits', irfs='P8R3_SOURCE_V2')
```

We can see the attributes of `obs` with a simple print statement:

```python
>>> print(obs)
Event file(s): TXS0506+056_select_gti.fits
Spacecraft file(s): ./spacecraft/L20040515476099D9076805_SC00.fits
Exposure map: TXS0506+056_expmap.fits
Exposure cube: TXS0506+056_ltcube.fits
IRFs: P8R3_SOURCE_V2
```

We now create `like`, a `UnbinnedAnalysis` object, which takes as attributes one `UnbinnedObs` object, the name of the XML model file and the name of the optimizer:

```python
>>> like = UnbinnedAnalysis(obs,'TXS0506+056_input_model.xml',optimizer='NewMinuit')
>>> print(like)
Event file(s): TXS0506+056_select_gti.fits
Spacecraft file(s): ./spacecraft/L20040515476099D9076805_SC00.fits
Exposure map: TXS0506+056_expmap.fits
Exposure cube: TXS0506+056_ltcube.fits
IRFs: P8R3_SOURCE_V2
Source model file: TXS0506+056_input_model.xml
Optimizer: NewMinuit
```

We can see the attributes of `like` with:

```python
>>> dir(like)
['NpredValue', 'Ts', 'Ts_old', '_Nobs', '__call__', '__class__', '__delattr__', '__dict__', '__doc__',
'__format__', '__getattribute__', '__getitem__', '__hash__', '__init__', '__module__', '__new__',
'__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__setitem__', '__sizeof__', '__str__',
'__subclasshook__', '__weakref__', '_errors', '_importPlotter', '_inputs', '_isDiffuseOrNearby',
'_minosIndexError', '_npredValues', '_plotData', '_plotResiduals', '_plotSource', '_renorm',
'_separation', '_setSourceAttributes', '_srcCnts', '_srcDialog', '_xrange', 'addGaussianPrior',
'addPrior', 'addPriors', 'addSource', 'constrain_norms', 'constrain_params', 'covar_is_current',
'covariance', 'deleteSource', 'disp', 'e_vals', 'energies', 'energyFlux', 'energyFluxError', 'fit',
 'flux', 'fluxError', 'freePars', 'freeze', 'getExtraSourceAttributes', 'getPriorLogDeriv',
 'getPriorLogValue', 'getPriors', 'logLike', 'maxdist', 'mergeSources', 'minosError', 'model',
 'nFreeParams', 'nobs', 'nobs_wt', 'normPar', 'numeric_deriv', 'observation', 'oplot', 'optObject',
  'optimize', 'optimizer', 'par_index', 'params', 'plot', 'plotSource', 'plotSourceFit',
  'readPriorsYaml', 'removePrior', 'removePriors', 'reset_ebounds', 'resids', 'restoreBestFit',
  'saveCurrentFit', 'scan', 'setEnergyRange', 'setFitTolType', 'setFreeFlag', 'setGaussianPriorParams', 
  'setPlotter', 'setPriorParams', 'setSpectrum', 'sourceFitPlots', 'sourceFitResids', 'sourceNames',
  'splitCompositeSource', 'srcModel', 'state', 'syncSrcParams', 'thaw', 'tol', 'tolType', 'total_nobs',
  'writeCountsSpectra', 'writePriorsYaml', 'writeXml']
```

The `like` object has several attributes, we can for example check the value of the tolerance which is used to check when to stop the maximization process:

```python
>>> like.tol
0.001
```

Now let's perform the fit/maximization process. We get the minimizing object, so that we can use it later, and we pass this object to the fit routine:

```python
>>> likeobj = pyLike.NewMinuit(like.logLike)
>>> like.fit(verbosity=3,covar=True,optObject=likeobj)

Minuit did successfully converge.
# of function calls: 92

47288.115129365004
```

Here we cut some output. The last number is the value of -log(likelihood), which we can use to compare the result obtained with this source model with another one. We can then plot the result of the fit

```python
>>> like.plot()
```

![TXS 0506+056 full model](./python_results/images/TXS0506+056_full_model.png)
![TXS 0506+056 residuals](./python_results/images/TXS0506+056_residuals.png)

We can also check if the optimizer did converge:

```python
>>> print likeobj.getRetCode()
0
```

An exit code of 0 means that the optimization process went well and converged. Now we can access the result of the fit by using `like.model`, which is a dictionary where the keys are the names of the sources in the XML input model and the values are represented by the values of the parameters for each source, according to their spectral model. Let's check the spectral parameters for TXS 0506+056:

```python
>>> like.model['TXS 0506+056']
TXS 0506+056
   Spectrum: LogParabola
561         norm:  2.034e+01  4.952e+00  1.000e-04  1.000e+04 ( 1.000e-12)
562        alpha:  2.055e+00  1.707e-01  0.000e+00  5.000e+00 ( 1.000e+00)
563         beta:  1.094e-01  1.168e-01  0.000e+00  1.000e+01 ( 1.000e+00)
564           Eb:  1.126e+03  0.000e+00  3.000e+01  5.000e+05 ( 1.000e+00) fixed
```

Beside the spectral parameters, one can access the flux value, the flux error and the TS:

```python
>>> like.flux('TXS 0506+056', emin=100)
2.043131594676075e-07
>>> like.fluxError('TXS 0506+056', emin=100)
5.7764877367108194e-08
>>> like.Ts('TXS 0506+056')
106.86425551313732
>>> import numpy as np
>>> np.sqrt(like.Ts('TXS 0506+056'))
10.337516893003722
```

We can see that the values we obtained with the python analysis are the same as the ones from the unbinned likelihood tutorial, and it should be so given that the data set, selection criteria and model are the same. Before proceeding further, we save the output model from the maximization process in a XML file:

```python
>>> like.logLike.writeXml('TXS0506+056_output_model.xml')
```

We can also have a look at the full model and residuals using `matplotlib`:

```python
>>> import matplotlib.pyplot as plt
>>> E = (like.energies[:-1] + like.energies[1:])/2.
>>> plt.figure(figsize=(5,5))
>>> plt.ylim((0.4,1e4))
(0.4, 10000.0)
>>> plt.xlim((100,300000))
(100, 300000)
>>> sum_model = np.zeros_like(like._srcCnts(like.sourceNames()[0]))
>>> for sourceName in like.sourceNames():
...     sum_model = sum_model + like._srcCnts(sourceName)
...     plt.loglog(E,like._srcCnts(sourceName),label=sourceName[1:])
...
>>> plt.loglog(E,sum_model,label='Total Model')
>>> plt.errorbar(E,like._Nobs(),yerr=np.sqrt(like._Nobs()), fmt='o',label='Counts')
<Container object of 3 artists>
>>> ax = plt.gca()
>>> box = ax.get_position()
>>> ax.set_position([box.x0, box.y0, box.width * 0.5, box.height])
>>> plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
>>> plt.show()
>>> resid = (like._Nobs() - sum_model)/sum_model
>>> resid_err = (np.sqrt(like._Nobs())/sum_model)
>>> plt.figure(figsize=(9,9))
>>> plt.xscale('log')
>>> plt.errorbar(E,resid,yerr=resid_err,fmt='o')
>>> plt.axhline(0.0,ls=':')
>>> plt.show()
```

![TXS 0506+056 full model](./python_results/images/TXS0506+056_full_model_matplotlib.png)
![TXS 0506+056 residuals](./python_results/images/TXS0506+056_residuals_matplotlib.png)

## Test Statistic maps ##

To create Test Statistic (TS) maps, we use the python object `TsMap`, which has attributes similar to the command line arguments given to `gttsmap`. We can create two source model files starting from the one we saved from running the likelihood analysis. One should have all the sources with no free parameters (let's call it `TXS0506+056_tsmap_residual.xml`), the second one should be the same as the former but with our source of interest, TXS 0506+056, removed from the model (let's call it `TXS0506+056_tsmap_source.xml`). We create two scripts, `pygttsmap_residual.py` and `pygttsmap_source.py`, as shown below:

```python
# pygttsmap_residual.py
import gt_apps as my_apps

my_apps.TsMap['statistic'] = "UNBINNED"
my_apps.TsMap['scfile']    = "./spacecraft/L20040515476099D9076805_SC00.fits"
my_apps.TsMap['evfile']    = "TXS0506+056_select_gti.fits"
my_apps.TsMap['expmap']    = "TXS0506+056_expmap.fits"
my_apps.TsMap['expcube']   = "TXS0506+056_ltcube.fits"
my_apps.TsMap['srcmdl']    = "TXS0506+056_tsmap_residual.xml"
my_apps.TsMap['irfs']      = "P8R3_SOURCE_V2"
my_apps.TsMap['optimizer'] = "NEWMINUIT"
my_apps.TsMap['outfile']   = "TXS0506+056_tsmap_residual.fits"
my_apps.TsMap['nxpix']     = 40
my_apps.TsMap['nypix']     = 40
my_apps.TsMap['binsz']     = 0.5
my_apps.TsMap['coordsys']  = "CEL"
my_apps.TsMap['xref']      = 77.3582
my_apps.TsMap['yref']      = 5.69315
my_apps.TsMap['proj']      = 'AIT'

my_apps.TsMap.run()
```

```python
# pygttsmap_source.py
import gt_apps as my_apps

my_apps.TsMap['statistic'] = "UNBINNED"
my_apps.TsMap['scfile']    = "./spacecraft/L20040515476099D9076805_SC00.fits"
my_apps.TsMap['evfile']    = "TXS0506+056_select_gti.fits"
my_apps.TsMap['expmap']    = "TXS0506+056_expmap.fits"
my_apps.TsMap['expcube']   = "TXS0506+056_ltcube.fits"
my_apps.TsMap['srcmdl']    = "TXS0506+056_tsmap_source.xml"
my_apps.TsMap['irfs']      = "P8R3_SOURCE_V2"
my_apps.TsMap['optimizer'] = "NEWMINUIT"
my_apps.TsMap['outfile']   = "TXS0506+056_tsmap_source.fits"
my_apps.TsMap['nxpix']     = 40
my_apps.TsMap['nypix']     = 40
my_apps.TsMap['binsz']     = 0.5
my_apps.TsMap['coordsys']  = "CEL"
my_apps.TsMap['xref']      = 77.3582
my_apps.TsMap['yref']      = 5.69315
my_apps.TsMap['proj']      = 'AIT'

my_apps.TsMap.run()
```

We can plot the resulting TS maps with `matplotlib` and `astropy`:

```python
import matplotlib.pyplot as plt
import astropy.io.fits as pyfits
residHDU = pyfits.open('TXS0506+056_tsmap_residual.fits')
sourceHDU = pyfits.open('TXS0506+056_tsmap_source.fits')
fig = plt.figure(figsize=(16,8))
plt.subplot(1,2,1)
plt.imshow(residHDU[0].data)
plt.colorbar()
plt.subplot(1,2,2)
plt.imshow(sourceHDU[0].data)
plt.colorbar()
plt.show()
```

The resulting plots are here below.

<img src="./likelihood_results/images/TXS0506+056_tsmaps.png" width="70%">

On the left there is the residual TS map. We see that it is pretty flat in TS space, meaning that no significant sources were left out from the model. On the right instead there is the source TS map. Here the most prominent source is TXS 0506+056 in the center.

## Creation of a Spectral Energy Distribution ##

Now we want to generate a Spectral Energy Distribution (SED). The generation of a SED requires to select the data in energy bins using `gtselect` and performing the likelihood fit for each energy bin. Among the user contributed software there is a script which handles this task, producing the spectral point and superimposing a butterfly plot. So, let's download the script:

```
[you@your_super_machine] $ wget https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/SED_scripts_v13.1.tgz
[you@your_super_machine] $ tar -zxvf SED_scripts_v13.1.tgz
[you@your_super_machine] $ cp SED_scripts_v13.1/likeSED.p* .
```

The script we need is called `likeSED.py`. Before doing anything, looking at the documentation of `likeSED.py`, that you can find at the link [https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/likeSEDmacros\_UsageNotes\_v13.pdf](https://fermi.gsfc.nasa.gov/ssc/data/analysis/user/likeSEDmacros\_UsageNotes\_v13.pdf), we can read that we should have a power law model for our source. Therefore, we simply copy the output model from the previous likelihood into a new file:

```
[you@your_super_machine] $ cp TXS0506+056_output_model.xml TXS0506+056_SED_model.xml
```

then we change the spectral model for TXS 0506+056:

```xml
<spectrum type="PowerLaw2">
    <parameter free="1" max="1e4" min="1e-4" name="Integral" scale="1e-7" value="4.73"/>
    <parameter free="1" max="0.0" min="-5.0" name="Index" scale="1.0" value="-2.0"/>
    <parameter free="0" max="5e5" min="30" name="LowerLimit" scale="1" value="1e2"/>
    <parameter free="0" max="5e5" min="30" name="UpperLimit" scale="1" value="3e5"/>
</spectrum>
```

We can now start using the script to produce the SED. This is a sample script, `LAT_SED.py`, which does exactly that:

```python
from likeSED import *
import pyLikelihood
from UnbinnedAnalysis import *

obs = UnbinnedObs('TXS0506+056_select_gti.fits','./spacecraft/L20040515476099D9076805_SC00.fits', expMap='TXS0506+056_expmap.fits', expCube='TXS0506+056_ltcube.fits', irfs='P8R3_SOURCE_V2')
like = UnbinnedAnalysis(obs,'TXS0506+056_SED_model.xml',optimizer='NewMinuit')

inputs = likeInput(like, 'TXS 0506+056', nbins=5)
inputs.plotBins()

inputs.fullFit(CoVar=True)
sed = likeSED(inputs)
sed.getECent()
sed.fitBands()
sed.Plot(plot=False)
```

A little bit of explanation:

* as for the likelihood, we need to create an `UnbinnedObs` and an `UnbinnedAnalysis` object, specifying for the latter the XML model with our source. These are used for the likelihood fit over the full energy range
* `inputs` is a `likeInput` object and it is set to use the `UnbinnedAnalysis` object defined before; it accepts a source name and we also define how many energy bins we want for our SED, 5 in this case
* `plotbins` just calculates the exposure map for each energetic band that we requested. It also chooses the highest energy bin to be that which contains the highest energy event found consistent with the position of your source within the 95% containment radius as defined by the instrument response functions (IRFs) specified in the UnbinnedObs object
* `fullFit` performs the fit for the full energy range
* a `likeSED` object is created from our input and the `getECent` method calculates the centers of our energy bins
* `fitBands` performs the likelihood fit for each energy band. An upper limit to the flux is calculated if in an energy bin the source has TS<25
* the `Plot` function creates three plots, the SED, the differential flux spectrum and histogram of the TS values in each energy band.

Here below you can find the output of the SED script:

```
Full energy range model for TXS 0506+056:
TXS 0506+056
   Spectrum: PowerLaw2
561     Integral:  2.340e+00  5.252e-01  1.000e-04  1.000e+04 ( 1.000e-07)
562        Index: -2.053e+00  1.376e-01 -5.000e+00  0.000e+00 ( 1.000e+00)
563   LowerLimit:  1.000e+02  0.000e+00  3.000e+01  5.000e+05 ( 1.000e+00) fixed
564   UpperLimit:  3.000e+05  0.000e+00  3.000e+01  5.000e+05 ( 1.000e+00) fixed

Flux 0.1-300.0 GeV 2.3e-07 +/- 5.3e-08 cm^-2 s^-1
Test Statistic 105.952335933

  -Runnng Likelihood for band0-

0 1.4268119124459453 -8.8003143901e-06 1.42681319246e-07
1 1.9975366774243235 0.374399885779 1.99755884938e-07
2 2.5682614424027017 1.43676999498 2.56832377019e-07
3 3.13898620738108 3.11264058752 3.13910090707e-07

    NOTE: Band0, with center energy 0.165034651386 GeV, quoting 95% upper limit on flux.
  -Runnng Likelihood for band1-


  -Runnng Likelihood for band2-
```

You can see that for the first energy bin an upper limit has been calculated, because TS is below 25 (actually, just below it)

At this point you should have the following files in your directory, after the SED creation:

* three EPS images corresponding to the previous plots
* one ROOT file where the plots are saved
* one FITS file in which all the data used to produce the plots are stored.

The tables needed for the plots production (except the TS one) are the following:

* the **DATA FLUX** table, with 5 columns:

   1. the center of the energy bins
   2. dN/dE in each energy bin
   3. the error on dN/dE
   4. E^2\*dN/dE in each energy bin
   5. the error on E^2\*dN/dE

* the **DATA ENERGY BINS** table, with 2 columns:

   1. the low edges of the bins
   2. the high edges of the bins

Here are the resulting plot from the SED calculation:

- SED

![TXS 0506+056 SED](./python_results/images/TXS_0506+056_5bins_EsqdNdE.png)

- differential flux spectrum

![TXS 0506+056 differential flux spectrum](./python_results/images/TXS_0506+056_5bins_cntSpec.png)

- TS vs energy plot

![TXS 0506+056 TS vs energy plot](./python_results/images/TXS_0506+056_5bins_TSvEnergy.png)
